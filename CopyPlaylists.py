import os
import glob
import re
import chardet
import ntpath
import shutil
import sys
from mutagen.mp3 import MP3
from mutagen.flac import FLAC

PLAYLISTS_IN_FOLDERS = False  # If true, write all songs of a playlist to it's own subfolder in SONG_DEST_DIR, and do not create M3U playlist file
# If false, all songs are written to the same folder (SONG_DEST_DIR), and an M3U file for each playlist is written to the same SONG_DEST_DIR



SOURCE_DIR = 'H:/Music/_Playlists/'
SONG_DEST_DIR = 'D:\Music'
SONG_PREFIX = '/storage/5C45-10DC/Music/'  # Where playlist will point to once on device, this is seperate from Song_dest_dir to allow copying over on another device
PLAYLIST_DEST_DIR = 'D:\Playlists'  # Where .M3U8 files and playlist subfolders are created


REMOVE_FILES = False

PLAYLIST_EXTENSION = '.mbp' # This is explicity designed to deal only with music bee playlists!
ENCODING = 'ISO-8859-2'  # Keep None to detect
SPECIAL_CHARS = ['°', '’', '“', '”', 'Ø', '‐', 'ò', 'é', 'ñ', '–', 'É']  # These characters have been manually searched and replaced
DRIVE_LETTER = SOURCE_DIR[0]

IGNORE_PLAYLISTS = []  # ['Top library tracks [overall]']


def _usage():
    print("Copies songs from a folder of .mbp playlists to a folder of folders, where each subfolder contains the files of a single playlist.\n"
          "Designed to work with MusicBee playlists ONLY (.mbp).\n"
          "Usage: CopyPlaylists playlists_folder destination_folder\n"
          "Note: Args can be set within script for convenience.")


def valid_char(c):
    """
    Checks that a character doesn't need any additional formatting.
    If a character is valid, it is either handled by a special case or can be handled without modification
    :param c: Input character
    :return: bool
    """
    assert len(c) == 1
    return 31 < ord(c) < 128 or c in SPECIAL_CHARS


class ProgressBar:
    """
    A visual aid to keep the user satisfied that a job is still making progress
    This is done by using a counting system
    """
    def __init__(self, start_val=0, min_val=0, max_val=100, increments=10, symbol='.', write_to_log=False, overhang=False):
        """
        :param start_val: The inital progress of the job, relative to min_val & max_val
        :param min_val: The low value value of the counter, if start_val = min_val then no progress has been made
        :param max_val: The terminative value of the counter, progress is complete when the counter reaches this value
        :param increments: The total number of symbols to be printed / the length of the progress bar
        :param symbol: The symbol to be used to fill the progress bar, must be str of len 1
        :param write_to_log: A bool which says whether or not to write the progress bar to the logger
        :param overhang, bool, Whether to print a bar above pb that indicates how long it'll be (for larger computations). Be warn of offsets
        """
        assert type(start_val) is int
        assert type(min_val) is int
        assert type(max_val) is int and max_val > min_val
        assert min_val <= start_val < max_val
        assert type(increments) is int
        assert type(symbol) is str and len(symbol) == 1
        assert type(write_to_log) is bool
        assert type(overhang) is bool
        if overhang:
            for i in range(increments):
                print('-', end='')
            print('', end='\n')
        self.symbol = symbol
        self.last_output = 0
        self.max = max_val
        self.min = min_val
        self.increments = (increments)
        self.cur_val = 0
        self.symbols_printed = 0
        self.write_to_log = write_to_log
        while start_val - self.last_output and start_val - self.last_output >= (
                    (self.max - self.min) / (1. / self.increments)):
            self.cur_val += ((self.max - self.min) / self.increments)
            self.tick()

    def update(self, new_val, set_val=True):
        """
        Updates the internal counter of ProgressBar
        :param new_val: The value to update with
        :param set_val: If True, set the internal counter to this value
        If False, add this value to the internal counter
        :return:
        """
        assert type(new_val) is int and (not set_val or new_val >= self.cur_val)
        assert type(set_val) is bool
        if not set_val:  # Then add
            if new_val < 0:
                raise Exception("Cannot increment progress bar by negative value:%s" % new_val)
            new_val = new_val + self.cur_val
        else:
            if new_val < self.cur_val:
                raise Exception("Cannot decrement progress bar value from %s to %s" %
                                (self.cur_val, new_val))
        while new_val - self.last_output >= ((self.max - self.min) / self.increments):
            self.cur_val += min(((self.max - self.min) / self.increments), new_val - self.cur_val)
            self.tick()
        self.cur_val = new_val

    def tick(self):
        """
        Prints one more symbol to indicate passing another interval
        :return:
        """
        if self.write_to_log:
            print(self.symbol, end='', flush=True)
        else:
            print(self.symbol, end='', flush=True)
        self.symbols_printed += 1
        self.last_output = self.cur_val

    def finish(self, newline=False):
        """
        All work is done, print any remaining symbols
        :param newline:Whether to terminate string with newline char
        :return:
        """
        assert type(newline) is bool
        symbols_before_finished = self.symbols_printed
        for i in range(int(self.increments) - symbols_before_finished):
            self.tick()
        if newline:
            print('', flush=True)
        else:
            print(' ', end='', flush=True)  # Add a space after the ticks


class Playlist:
    def __init__(self, path):
        self.path = path
        self.files = self.read()
        self.name = self.gen_name()
        self.m3u8 = self.gen_m3u8()

    def read(self, encoding=ENCODING):
        f = open(self.path, 'rb')
        raw = f.read()

        if encoding is None or (type(encoding) is str and len(encoding) == 0):
            encoding = chardet.detect(raw)['encoding']
        line = raw.decode(encoding=encoding)

        # Need to fix formatting, suspect that musicbee uses a custom encoding, which can't be automatically applied. Closest to ISO-8859-2
        line = re.sub('â', "’", line)  # Angled single quote
        line = re.sub('â', "“", line)  # Opening double quote
        line = re.sub('â', "”", line)  # Closing double quote
        line = re.sub('Ă', 'Ø', line)
        line = re.sub('â', '‐', line)
        line = re.sub('Ă˛', 'ò', line)
        line = re.sub('ĂŠ', 'é', line)
        line = re.sub('Ăą', 'ñ', line)
        line = re.sub('â', '–', line)  # Short hyphen
        line = re.sub('Â°', '°', line)
        line = re.sub('Ă', 'É', line)

        # Need to remove preceding text before song titles:
        line = line[line.find(':') + 1:]
        line = line[line.find(':') - 1:]
        last_char_ascii = True
        cur_path = ''
        song_paths = []

        for index, char in enumerate(line):

            if valid_char(char) or (index + 3 < len(line) and line[index + 1:index + 3] == (DRIVE_LETTER + ':')):
                if last_char_ascii:  # This is because there is usually a trailing character following the extension of the previous song
                    cur_path += char
                else:
                    # Need to check for the rare case there isn't a buffer ascii-character
                    song_paths.append(cur_path)
                    cur_path = ''
                last_char_ascii = True
            else:
                last_char_ascii = False

        if len(song_paths) and song_paths[0] == '':  # HACK
            song_paths = song_paths[1:]  # Due to formatting oocasionally having a : right before first path

        for index, song in enumerate(song_paths):
            songs_to_remove = []
            if not os.path.isfile(song):
                # So far this is only due to difficulties isolating ' and ’

                new_name = re.sub("'", "’", song)
                new_file = glob.glob(new_name)
                if len(new_file):
                    print(" Adjusting %s to %s" % (song, new_file[0]))
                    song_paths[index] = new_file[0]
                else:
                    print(" Couldn't locate song at path:\"%s\"" % song)
            if len(songs_to_remove):
                print("  Removing the following songs which couldn't be found: %s" % songs_to_remove)
            for song in songs_to_remove:
                song_paths.remove(song)
        return song_paths

    def gen_name(self):
        return self.path[self.path.rfind('\\')+1: self.path.rfind(PLAYLIST_EXTENSION)]

    def gen_m3u8(self):
        res = '#EXTM3U\n'
        for song in self.files:
            extension = os.path.splitext(song)[1][1:]
            if extension == 'mp3':
                length = MP3(song).info.length
            elif extension == 'flac':
                length = FLAC(song).info.length
            res += "#EXTINF:%s,%s" % (int(length), os.path.basename(song)) + '\n' + os.path.join(SONG_PREFIX, os.path.basename(song)) + '\n'
        return res

    def write_m3u8(self):
        dest = os.path.join(PLAYLIST_DEST_DIR, self.name + '.m3u')
        if os.path.exists(dest):
            print("Overwriting playlist:%s" % dest)
        f = open(dest, 'w')
        f.write(self.m3u8)
        f.close()

    def __str__(self):
        return "Playlist(Name:%s, Path:%s, file_count:%s)" % (self.name, self.path, len(self.files))

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def get_playlists(directory=SOURCE_DIR):
        directory = os.path.join(directory, '')
        playlist_paths = glob.glob(directory + '*' + PLAYLIST_EXTENSION)
        playlists = [Playlist(playlist_path) for playlist_path in playlist_paths]
        res = []
        for playlist in playlists:
            if not len(playlist.files):
                print("Skipping empty playlist:%s" % playlist)
            else:
                res.append(playlist)
        return res


if __name__ == '__main__':

    if len(sys.argv) > 1:
        if len(sys.argv) != 3:
            _usage()
            exit(1)
        else:
            SOURCE_DIR = sys.argv[1]
            SONG_DEST_DIR = sys.argv[2]
    if not os.path.exists(SOURCE_DIR):
        raise Exception("Source folder:'%s' does not exist" % SOURCE_DIR)
    if not os.path.exists(SONG_DEST_DIR):
        raise Exception("Song destination folder:'%s' does not exist" % SONG_DEST_DIR)
    if not os.path.exists(PLAYLIST_DEST_DIR):
        raise Exception("Playlist destination folder:'%s' does not exists" % PLAYLIST_DEST_DIR)

    playlists = Playlist.get_playlists()

    if PLAYLISTS_IN_FOLDERS:
        for playlist_index, playlist in enumerate(playlists):
            print("Processing playlist #%s/%s = %s" % (playlist_index, len(playlists), playlist.name))
            if playlist.name in IGNORE_PLAYLISTS:
                print(" Skipping")
            else:
                print(" Playlist files(%s):%s" % (len(playlist.files), [ntpath.basename(file) for file in playlist.files]))
                dest_playlist_dir = os.path.join(PLAYLIST_DEST_DIR, playlist.name)

                if not os.path.exists(dest_playlist_dir):
                    print(" Creating dir:%s" % dest_playlist_dir)
                    os.makedirs(dest_playlist_dir)

                if REMOVE_FILES and len(os.listdir(dest_playlist_dir)):
                    print(" Removing files from dir:%s" % dest_playlist_dir)
                    remove_files = os.listdir(dest_playlist_dir)
                    for file in remove_files:
                        os.unlink(os.path.join(dest_playlist_dir, file))

                print(" Copying to dir:%s" % dest_playlist_dir, end=' ')
                dest_basenames = [ntpath.basename(file) for file in os.listdir(dest_playlist_dir)]
                files_still_to_be_copied = [file for file in playlist.files if ntpath.basename(file) not in dest_basenames]
                if not len(files_still_to_be_copied):
                    print("- All files already copied")
                else:
                    print("%s files to copy " % len(files_still_to_be_copied), end='')
                    pb = ProgressBar(max_val=len(files_still_to_be_copied), increments=20)
                    for file_index, file in enumerate(files_still_to_be_copied):
                        base_filename = ntpath.basename(file)
                        shutil.copy(file, os.path.join(dest_playlist_dir, base_filename))
                        pb.update(file_index, set_val=True)
                    pb.finish()
            print('')
    else:
        all_songs = set(song for playlist in playlists for song in playlist.files)
        dest_contents = glob.glob(os.path.join(SONG_DEST_DIR, '*'))
        dest_files_basenames = [os.path.basename(file) for file in dest_contents if os.path.isfile(file)]
        songs_to_sync = [song for song in all_songs if os.path.basename(song) not in dest_files_basenames]

        if len(songs_to_sync):
            print("Number of songs not already synced: %s" % len(songs_to_sync))
            print('-' * 50)
            pb = ProgressBar(max_val=len(songs_to_sync), increments=50)
            for num, song in enumerate(songs_to_sync):
                base_filename = os.path.basename(song)
                # print("Syncing song: %s, basename:%s" % (song, base_filename))
                shutil.copy(song, os.path.join(SONG_DEST_DIR, base_filename))
                pb.update(num, set_val=True)
            pb.finish()
            print("Done copying songs, now writing playlists")
        else:
            print("All songs already synced")
        for playlist in playlists:
            print("Writing playlist:%s as M3U8 to %s" % (playlist, os.path.join(PLAYLIST_DEST_DIR, playlist.name + '.m3u')))
            # print(playlist.m3u8)
            playlist.write_m3u8()
            print('')